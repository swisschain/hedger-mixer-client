﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Swisschain.Hedger.Mixer.ApiClient;
using Swisschain.Hedger.Mixer.ApiContract;

namespace TestClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var url = "localhost";
            //var url = "hedger-mixer.services.svc.cluster.local";

            var client = new MixerClient($"http://{url}:5001");

            //await IsAlive(client);

            await OrderBooksStream(client);
        }

        private static async Task OrderBooksStream(MixerClient client)
        {
            while (true)
            {
                try
                {
                    var request = new GetOrderBooksRequest();

                    using var orderBooks = client.OrderBooks.GetOrderBooks(request);

                    await foreach (var item in orderBooks.ResponseStream.ReadAllAsync())
                    {
                        Console.WriteLine($"{item.Source}-{item.AssetPairId}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Thread.Sleep(1000);
            }
        }

        private static async Task IsAlive(MixerClient client)
        {
            while (true)
            {
                try
                {
                    var sw = new Stopwatch();
                    sw.Start();
                    var result = await client.Monitoring.IsAliveAsync(new IsAliveRequest());
                    sw.Stop();
                    Console.WriteLine($"{result.Name}  {sw.ElapsedMilliseconds} ms");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Thread.Sleep(1000);
            }
        }
    }
}
