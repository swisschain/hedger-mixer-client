﻿using Swisschain.Hedger.Mixer.ApiClient.Common;
using Swisschain.Hedger.Mixer.ApiContract;

namespace Swisschain.Hedger.Mixer.ApiClient
{
    public class MixerClient : BaseGrpcClient, IMixerClient
    {
        public MixerClient(string serverGrpcUrl) : base(serverGrpcUrl)
        {
            Monitoring = new Monitoring.MonitoringClient(Channel);
            OrderBooks = new OrderBooksGrpc.OrderBooksGrpcClient(Channel);
        }

        public Monitoring.MonitoringClient Monitoring { get; }
        public OrderBooksGrpc.OrderBooksGrpcClient OrderBooks { get; }
    }
}
