﻿using Swisschain.Hedger.Mixer.ApiContract;

namespace Swisschain.Hedger.Mixer.ApiClient
{
    public interface IMixerClient
    {
        Monitoring.MonitoringClient Monitoring { get; }
        OrderBooksGrpc.OrderBooksGrpcClient OrderBooks { get; }
    }
}
